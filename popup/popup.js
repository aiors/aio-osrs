function isInt(value) {
  return !isNaN(value) &&
         parseInt(Number(value)) == value &&
         !isNaN(parseInt(value, 10));
}

let att = 1
let str = 1
let def = 1
let rng = 1
let mgc = 1
let pray = 1
let hp = 10

let cmblvl = 0
let base = 0
let melee = 0
let range = 0
let magic = 0
let max = 0

function calcCmb() {
	base = 0.25 * (def + hp + Math.floor(pray/2))
	melee = 0.325 * (att + str)
	range = 0.325 * (Math.floor(rng/2) + rng)
	magic = 0.325 * (Math.floor(mgc/2) + mgc)

	const skills = [melee, range, magic]

	max = skills.reduce((a, b) => {
		return Math.max(a, b)
	})

	cmblvl = Number(Math.round((base + max)+'e2')+'e-2')

	$('#lvl').text(cmblvl)
}

function calcNextAtt() {
	if (cmblvl == 126 || att == 99)
		return 0

	let attlvls = Math.floor(cmblvl + 1)
	attlvls = attlvls - base
	attlvls = Math.ceil((attlvls / 0.325) - str)

	return (attlvls - att) >= 99 ? 99 : (attlvls - att)
}

function calcNextStr() {
	if (cmblvl == 126 || str == 99)
		return 0

	let strlvls = Math.floor(cmblvl + 1)
	strlvls = strlvls - base
	strlvls = Math.ceil((strlvls / 0.325) - att)

	return (strlvls - str) >= 99 ? 99 : (strlvls - str)
}

function calcNextDef() {
	if (cmblvl == 126 || def == 99)
		return 0

	let deflvls = Math.floor(cmblvl + 1)
	deflvls = deflvls - max
	deflvls = Math.ceil(deflvls * 4 - hp - Math.floor(pray/2))

	return (deflvls - def) >= 99 ? 99 : (deflvls - def)
}

function calcNextHp() {
	if (cmblvl == 126 || hp == 99)
		return 0

	let hplvls = Math.floor(cmblvl + 1)
	hplvls = hplvls - max
	hplvls = Math.ceil(hplvls * 4 - def - Math.floor(pray/2))

	return (hplvls - hp) >= 99 ? 99 : (hplvls - hp)
}

function calcNextPray() {
	if (cmblvl == 126 || pray == 99)
		return 0

	let praylvls = Math.floor(cmblvl + 1)
	praylvls = praylvls - max
	praylvls = Math.ceil(Math.ceil(praylvls * 4 - def - hp)*2)

	return (praylvls - pray) >= 99 ? 99 : (praylvls - pray)
}

function calcNextMagic() {
	if (cmblvl == 126 || mgc == 99)
		return 0

	let magiclvls = Math.floor(cmblvl + 1)
	magiclvls = magiclvls - base
	magiclvls = Math.ceil(magiclvls / 0.325 * 2 / 3)

	return (magiclvls - mgc) >= 99 ? 99 : (magiclvls - mgc)
}

function calcNextRange() {
	if (cmblvl == 126 || rng == 99)
		return 0

	let rangelvls = Math.floor(cmblvl + 1)
	rangelvls = rangelvls - base
	rangelvls = Math.ceil(rangelvls / 0.325 * 2 / 3)

	return (rangelvls - rng) >= 99 ? 99 : (rangelvls - rng)
}

function calcAll() {
	calcCmb()
	$('#tnlatt').text(calcNextAtt())
	$('#tnlstr').text(calcNextStr())
	$('#tnldef').text(calcNextDef())
	$('#tnlmgc').text(calcNextMagic())
	$('#tnlrng').text(calcNextRange())
	$('#tnlhp').text(calcNextHp())
	$('#tnlpray').text(calcNextPray())
}


$(document).ready(function() {
	calcAll()

	$('.combat').hide()

	$('#gotonews').click(() => {
		const url =  'http://services.runescape.com/m=news/archive?oldschool=1'
		chrome.tabs.create({url: url})
	})


	$('#gotohighscores').click(() => {
		const url = 'http://services.runescape.com/m=hiscore_oldschool/overall.ws'
		chrome.tabs.create({url: url})
	})


	$('#gotoefficiency').click(() => {
		const url = 'https://rsbuddy.com/efficiency'
		chrome.tabs.create({url: url})
	})


	$('#gotocml').click(() => {
		chrome.storage.sync.get('cml', function(links) {
			let url = ''

			if (links.cml) {
				url = 'https://crystalmathlabs.com/tracker/track.php?player=' + encodeURI(links.cml)
			} else {
				url = 'https://crystalmathlabs.com/tracker/'
			}

			chrome.tabs.create({url: url})
		})
	})


	$('#gotoreddit').click(() => {
		const url = 'https://www.reddit.com/r/2007scape/'
		chrome.tabs.create({url: url})
	})


	$('#gotodonate').click(() => {
		const url = 'https://paypal.me/jasonou'
		chrome.tabs.create({url: url})
	})


	$('#gotocombat').click(() => {
		$('.links').hide()
		$('.search').hide()

		$('.combat').show()
	})


	$('#gotoback').click(() => {
		$('.links').show()
		$('.search').show()

		$('.combat').hide()
	})

	$('#clear').click(() => {
		att = 1
		str = 1
		def = 1
		rng = 1
		mgc = 1
		pray = 1
		hp = 10

		calcAll()

		$('#attack').val('')
		$('#strength').val('')
		$('#defence').val('')
		$('#magic').val('')
		$('#range').val('')
		$('#prayer').val('')
		$('#hitpoints').val('')
	})


	$('#attack').keyup(() => {
		const val = $('#attack').val()
		if (isInt(val) && val >= 1 && val <= 99)
			att = parseInt(val)
		else if (isInt(val) && val > 99)
			att = 99
		else
			att = 1
		calcAll()
	})

	$('#strength').keyup(() => {
		const val = $('#strength').val()
		if (isInt(val) && val >= 1 && val <= 99)
			str = parseInt(val)
		else if (isInt(val) && val > 99)
			str = 99
		else
			str = 1
		calcAll()
	})

	$('#defence').keyup(() => {
		const val = $('#defence').val()
		if (isInt(val) && val >= 1 && val <= 99)
			def = parseInt(val)
		else if (isInt(val) && val > 99)
			def = 99
		else
			def = 1
		calcAll()
	})

	$('#range').keyup(() => {
		const val = $('#range').val()
		if (isInt(val) && val >= 1 && val <= 99)
			rng = parseInt(val)
		else if (isInt(val) && val > 99)
			rng = 99
		else
			rng = 1
		calcAll()
	})

	$('#magic').keyup(() => {
		const val = $('#magic').val()
		if (isInt(val) && val >= 1 && val <= 99)
			mgc = parseInt(val)
		else if (isInt(val) && val > 99)
			mgc = 99
		else
			mgc = 1
		calcAll()
	})

	$('#prayer').keyup(() => {
		const val = $('#prayer').val()
		if (isInt(val) && val >= 1 && val <= 99)
			pray = parseInt(val)
		else if (isInt(val) && val > 99)
			pray = 99
		else
			pray = 1
		calcAll()
	})

	$('#hitpoints').keyup(() => {
		const val = $('#hitpoints').val()
		if (isInt(val) && val >= 10 && val <= 99)
			hp = parseInt(val)
		else if (isInt(val) && val > 99)
			hp = 99
		else
			hp = 10
		calcAll()
	})
})
