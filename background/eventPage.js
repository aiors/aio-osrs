var newsItem = {
	"id": "getNews",
	"title": "News",
	"contexts": ["all"]
}

chrome.contextMenus.create(newsItem)

var hiscoresItem = {
	"id": "getHiscores",
	"title": "Hiscores",
	"contexts": ["all"]
}

chrome.contextMenus.create(hiscoresItem)

var ehpItem = {
	"id": "getEhp",
	"title": "EHP",
	"contexts": ["all"]
}

chrome.contextMenus.create(ehpItem)

var redditItem = {
	"id": "getReddit",
	"title": "Reddit",
	"contexts": ["all"]
}

chrome.contextMenus.create(redditItem)

var skillsItem = {
	"id": "getSkills",
	"title": "Skills Calc",
	"contexts": ["all"]
}

chrome.contextMenus.create(skillsItem)


chrome.contextMenus.onClicked.addListener(function(data) {
	let url = ''

	if (data.menuItemId == "getNews") {
		url =  'http://services.runescape.com/m=news/archive?oldschool=1'
		chrome.tabs.create({url: url})
	} else if (data.menuItemId == "getHiscores") {
		url = 'http://services.runescape.com/m=hiscore_oldschool/overall.ws'
		chrome.tabs.create({url: url})
	} else if (data.menuItemId == "getEhp") {
		chrome.storage.sync.get('cml', function(links) {
			if (links.cml) {
				url = 'https://crystalmathlabs.com/tracker/track.php?player=' + encodeURI(links.cml)
			} else {
				url = 'https://crystalmathlabs.com/tracker/'
			}

			chrome.tabs.create({url: url})
		})
	} else if (data.menuItemId == "getReddit") {
		url = 'https://www.reddit.com/r/2007scape/'
		chrome.tabs.create({url: url})
	} else if (data.menuItemId == "getSkills") {
		url = 'https://rsbuddy.com/efficiency'
		chrome.tabs.create({url: url})
	}
})