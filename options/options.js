$(function() {
	chrome.storage.sync.get('cml', function(links) {
		if (links.cml)
			$('#cmlacc').val(links.cml)
	})

	$('#cmlsubmit').click(function() {
		var account = $('#cmlacc').val()
		chrome.storage.sync.set({'cml': account}, function() {
			close()
		})
	})


	$('#cmlacc').on("keyup", function(e) {
		if (e.keyCode == 13) {
			var account = $('#cmlacc').val()
			chrome.storage.sync.set({'cml': account}, function() {
				close()
			})
		}
	})
})
