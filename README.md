# All in one RuneScape resources manager

> AIO Old School RuneScape Resources Manager Chrome Extension

> All in one (2007) RuneScape resources manager.

## FEATURES

[Youtube Video](https://www.youtube.com/watch?time_continue=14&v=fm88EQ9JJ5g)

### Easy access to common resources

1) Combat Calculator
2) EHP
3) Hiscores
4) News
5) Reddit
6) Skills Calculator

> Instant lookup for OSRS Wiki

> Instant lookup for OSRS Grand Exchange

> Right click anywhere and have quick resource links displayed

> Set your account for Crystal Math Labs


## FUTURE UPDATES

> Fetching user from hiscores for combat level calculator

> Ability to choose what links you want displayed

> Old School RuneScape Inventory theme

## DISCLAIMER:

> "Copyright Disclaimer Under Section 107 of the Copyright Act 1976, allowance is made for "fair use" for purposes such as criticism, comment, news reporting, teaching, scholarship, and research. Fair use is a use permitted by copyright statute that might otherwise be infringing. Non-profit, educational or personal use tips the balance in favor of fair use."

> This extension is made by me, Jason Ou and this is in no way shape or form affiliated to Jagex or their products.
RuneScape® is a registered trademark of Jagex Ltd © 1999 - 2017.

> http://www.runescape.com